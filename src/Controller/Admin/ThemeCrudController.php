<?php

namespace App\Controller\Admin;

use App\Entity\Theme;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;

class ThemeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Theme::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            ChoiceField::new('theme', 'Тема')->setChoices([
                'Yellow' => 'yellow',
                'Yellow Light' => 'yellow-light',
                'Red' => 'red',
                'Purple' => 'purple',
                'Olive' => 'olive',
                'Midnight Blue' => 'midnight-blue',
                'Green Light 2' => 'green-light-2',
                'Green Light' => 'green-light',
                'Green' => 'green',
                'Brown' => 'brown',
                'Blue' => 'blue'
            ])
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->remove(Crud::PAGE_INDEX, Action::NEW)
            ->remove(Crud::PAGE_INDEX, Action::DELETE);
    }
}
