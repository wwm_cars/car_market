<?php

namespace App\Controller;

use App\Entity\Banner;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class BannerController  extends AbstractController
{
    public function findByPlace(string $place)
    {
        $banners = $this->getDoctrine()
            ->getRepository(Banner::class)
            ->findBy(['place' => $place], ['show_order' => 'ASC']);
        $images = [];
        /**@var $banner Banner*/
        foreach ($banners as $banner) {
            $images[]= $banner->getImage();
        }

        return new Response(implode(';;', $images));
    }

    public function findOneByPlace(string $place)
    {
        $banner = $this->getDoctrine()
            ->getRepository(Banner::class)
            ->findOneBy(['place' => $place]);
        $images = [];
        /**@var $banner Banner*/

        return new Response($banner != null ? $banner->getImage(): null);
    }
}