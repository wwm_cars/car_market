<?php

namespace App\Controller\Admin;

use App\Entity\Car;
use App\Form\CharacteristicType;
use App\Form\ImageType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CarCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Car::class;
    }

    public function configureFields(string $pageName): iterable
    {
        $startYear = (int) date('Y') - 40;
        $endYear = (int) date('Y');

        $fields = [
            IntegerField::new('diller_id'),
            TextField::new('brand'),
            TextField::new('model'),
            TextField::new('equipment'),
            IntegerField::new('price'),
            ChoiceField::new('year')->setChoices(
                array_combine(range($startYear, $endYear), range($startYear, $endYear))
            ),
            ChoiceField::new('status')->setChoices([
                'Ничего' => 0,
                'В тренде' => 1,
                'Новинки' => 2,
                'Выбор клиентов' => 3,
                'Премиум авто' => 4,
            ])->hideOnIndex(),
            TextField::new('transmission')->setRequired(false)->hideOnIndex(),
            TextField::new('color')->setRequired(false),
            TextField::new('fuel')->setRequired(false),
            IntegerField::new('way')->setRequired(false)->hideOnIndex(),
            TextField::new('body_type')->setRequired(false),
            TextField::new('type')->setRequired(false),
            TextareaField::new('description')->setRequired(false)->hideOnIndex(),
            DateTimeField::new('created_at')->hideOnForm(),
            DateTimeField::new('updated_at')->hideOnForm()->hideOnIndex(),
        ];

        if ($pageName !== Crud::PAGE_NEW) {
            $fields = array_merge([IdField::new('id')->setDisabled()], $fields);
        }

        if (in_array($pageName, [Crud::PAGE_EDIT, Crud::PAGE_NEW])) {
            $fields[] = AssociationField::new('relatedCars', 'Привязанные машины')->hideOnIndex();
            $fields[] = AssociationField::new('carsWithCurrent', 'Привязан к')->hideOnIndex();

            $fields[] = CollectionField::new('characteristics')
                ->setEntryType(CharacteristicType::class)
                ->setFormTypeOption('by_reference', false);

            $fields[] = CollectionField::new('images')
                ->setEntryType(ImageType::class)
                ->setFormTypeOption('by_reference', false)
                ->onlyOnForms();
        } elseif ($pageName === Crud::PAGE_DETAIL) {
            $fields[] = CollectionField::new('relatedCars', 'Привязанные машины')
                ->setTemplatePath('bundles/EasyAdminBundle/related_cars.html.twig');
            $fields[] = CollectionField::new('carsWithCurrent', 'Привязан к')
                ->setTemplatePath('bundles/EasyAdminBundle/related_cars.html.twig');
            $fields[] = CollectionField::new('characteristics')
                ->setTemplatePath('bundles/EasyAdminBundle/car_characteristics.html.twig');
            $fields[] = CollectionField::new('images')
                ->setTemplatePath('bundles/EasyAdminBundle/car_images.html.twig');
        }

        if ($pageName == Crud::PAGE_INDEX) {
            $fields[] = CollectionField::new('images')
                ->setTemplatePath('bundles/EasyAdminBundle/car_index_images.html.twig');
        }

        return $fields;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('brand')
            ->add('model')
            ->add('equipment')
            ->add('year')
            ->add('fuel')
            ->add('price')
            ->add('status')
            ->add('created_at');
    }
}
