<?php

namespace App\Controller;

use App\Entity\Car;
use App\Entity\Characteristic;
use App\Repository\CarRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DateTimeImmutable;

class ProcessDataController extends AbstractController
{
    /**
     */
    public function index1()
    {
        $handle = fopen(__DIR__.'/1.csv', "r");

        $keys = [];
        $isKeys = true;
        while (($data = fgetcsv($handle, 0, ';')) !== false) {
            if (empty($data[1])) {
                continue;
            } elseif ($isKeys) {
                $isKeys = false;
                $keys = $data;
                continue;
            }
            $car = new Car();
            $car->setDillerId((int)$data[0]);
            $car->setBrand($data[1]);
            $car->setModel($data[2]);
            $car->setEquipment($data[3]);
            $car->setPrice((int)$data[6]);
            $car->setYear((int)$data[9]);
            $car->setFullData($this->generateFullData($keys, $data));
            $car->setStatus(0);
            $car->setTransmission('-');
            $car->setColor('цвет');
            $car->setFuel('electro');
            $car->setWay(0);
            $car->setBodyType('седан');
            $car->setType('новый');
            $car->setCreatedAt(new DateTimeImmutable());
            $car->setUpdatedAt(new DateTimeImmutable());

            $carRepository = $this->getDoctrine()->getRepository(Car::class);
            $carRepository->save($car);

        }

        return new Response('<h1>Data saved</h1>');
    }

    private function generateFullData(array $keys, array $data): array
    {
        $fullData = [];
        foreach ($data as $i=>$field) {
            $fullData[$keys[$i]] = $field;
        }

        return $fullData;
    }

    /**
     * @Route("/process/characteristics")
     */
    public function index()
    {
        /** @var CarRepository $carRepository */
        $carRepository = $this->getDoctrine()->getRepository(Car::class);
        $cars = $carRepository->findDataForFillCharacteristics();

        $em = $this->getDoctrine()->getManager();
        foreach ($cars as $id => $fullData) {
            $car = $carRepository->findOneBy(['id' => $id]);

            foreach ($fullData as $key => $value) {
                if ($value === '' or in_array($key, ['', 'ВЕРСІЯ', 'МОДЕЛЬ', 'ВИРОБНИК', 'ФОТОАЛЬБОМ*', 'МОДЕЛЬНИЙ РІК', 'Модель+версия'])) {
                    continue;
                }

                $characteristic = new Characteristic();
                $characteristic->setName($key);
                $characteristic->setValue($value);
                $characteristic->setCar($car);
                $em->persist($characteristic);
            }
        }

        $em->flush();
        $em->clear();

        return new Response('<h1>Data updated</h1>');
    }
}
