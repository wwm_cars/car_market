<?php

namespace App\Controller;

use App\Entity\Theme;
use App\Repository\BannerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InfoController extends AbstractController
{
    /**
     * @Route("/about_us", name="aboutUs")
     */
    public function loadAboutUs(BannerRepository $bannerRepository): Response
    {
        return $this->render('info/aboutUs.html.twig', [
            'Theme' => $this->loadTheme()
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function loadContact(): Response
    {
        return $this->render('info/contact.html.twig', [
            'Theme' => $this->loadTheme()
        ]);
    }

    private function loadTheme(): string
    {
        /** @var Theme $theme */
        $theme = $this->getDoctrine()
            ->getRepository(Theme::class)
            ->findOneBy([], ['id' => 'ASC']);
        if ($theme) {
            return $theme->getTheme();
        }
        return 'yellow';
    }
}
