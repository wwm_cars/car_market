<?php

namespace App\Entity;

use App\Repository\CarRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CarRepository::class)
 */
class Car
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $diller_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $brand;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $model;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $equipment;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     */
    private $year;

    /**
     * @ORM\Column(type="json")
     */
    private $full_data = [];

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $transmission;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $color;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fuel;

    /**
     * @ORM\Column(type="integer")
     */
    private $way;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $body_type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="car", orphanRemoval=true, cascade={"persist"})
     */
    private $images;

    /**
     * Машины к которым привязали текущую
     *
     * @ORM\ManyToMany(targetEntity="Car", mappedBy="relatedCars")
     */
    private $carsWithCurrent;

    /**
     * Привязанные машины к текущей
     *
     * @ORM\ManyToMany(targetEntity="Car", inversedBy="carsWithCurrent")
     * @ORM\JoinTable(name="related_car",
     *      joinColumns={@ORM\JoinColumn(name="car_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="related_car_id", referencedColumnName="id")}
     *      )
     */
    private $relatedCars;

    /**
     * @var \DateTime
     * @Gedmo\Mapping\Annotation\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @var \DateTime
     * @Gedmo\Mapping\Annotation\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity=Characteristic::class, mappedBy="car", orphanRemoval=true, cascade={"persist"})
     * @ORM\OrderBy({"name" = "DESC"})
     */
    private $characteristics;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $powerReserve;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $batteryCapacity;

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->characteristics = new ArrayCollection();
        $this->transmission = '';
        $this->color = '';
        $this->fuel = '';
        $this->way = 0;
        $this->body_type = '';
        $this->type = '';
        $this->description = '';
        $this->relatedCars = new ArrayCollection();
        $this->carsWithCurrent = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDillerId(): ?int
    {
        return $this->diller_id;
    }

    public function setDillerId(int $diller_id): self
    {
        $this->diller_id = $diller_id;

        return $this;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getEquipment(): ?string
    {
        return $this->equipment;
    }

    public function setEquipment(string $equipment): self
    {
        $this->equipment = $equipment;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getFullData(): ?array
    {
        return $this->full_data;
    }

    public function setFullData(array $full_data): self
    {
        $this->full_data = $full_data;

        return $this;
    }

    public function getStatus():?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getTransmission(): ?string
    {
        return $this->transmission;
    }

    public function setTransmission(?string $transmission): self
    {
        if ($transmission !== null) {
            $this->transmission = $transmission;
        }

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        if ($color !== null) {
            $this->color = $color;
        }

        return $this;
    }

    public function getFuel(): ?string
    {
        return $this->fuel;
    }

    public function setFuel(?string $fuel): self
    {
        if ($fuel !== null) {
            $this->fuel = $fuel;
        }

        return $this;
    }

    public function getWay(): ?int
    {
        return $this->way;
    }

    public function setWay(?int $way): self
    {
        if ($way !== null) {
            $this->way = $way;
        }

        return $this;
    }

    public function getBodyType(): ?string
    {
        return $this->body_type;
    }

    public function setBodyType(?string $body_type): self
    {
        if ($body_type !== null) {
            $this->body_type = $body_type;
        }

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        if ($type !== null) {
            $this->type = $type;
        }

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setCar($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getCar() === $this) {
                $image->setCar(null);
            }
        }

        return $this;
    }

    /**
     * @return Characteristic[]|Collection
     */
    public function getCharacteristics(): Collection
    {
        return $this->characteristics;
    }

    public function __toString() {
        $text = '';
        if (isset($this->id) and $this->id > 0) {
            $text .= $this->id.'#'.' ';
        }
        return $text.$this->brand.' '.$this->model.' '.$this->equipment.' '.$this->year;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Image|null
     */
    public function getAvatar(): ?Image
    {
        foreach ($this->getImages() as $image) {
            if ($image->isAvatar()) {
                return $image;
            }
        }
        return null;
    }

    public function addCharacteristic(Characteristic $characteristic): self
    {
        if (!$this->characteristics->contains($characteristic)) {
            $this->characteristics[] = $characteristic;
            $characteristic->setCar($this);
        }

        return $this;
    }

    public function removeCharacteristic(Characteristic $characteristic): self
    {
        if ($this->characteristics->removeElement($characteristic)) {
            // set the owning side to null (unless already changed)
            if ($characteristic->getCar() === $this) {
                $characteristic->setCar(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        if (null !== $description) {
            $this->description = $description;
        }

        return $this;
    }

    /**
     * @return Collection|Car[]
     */
    public function getRelatedCars(): Collection
    {
        return $this->relatedCars;
    }

    public function addRelatedCar(Car $relatedCar): self
    {
        if (!$this->relatedCars->contains($relatedCar)) {
            $this->relatedCars[] = $relatedCar;
            $relatedCar->addRelatedCar($this);
        }

        return $this;
    }

    public function removeRelatedCar(Car $relatedCar): self
    {
        if ($this->relatedCars->contains($relatedCar)) {
            $this->relatedCars->removeElement($relatedCar);
            $relatedCar->removeRelatedCar($this);
        }

        return $this;
    }

    /**
     * @return Collection|Car[]
     */
    public function getCarsWithCurrent(): Collection
    {
        return $this->carsWithCurrent;
    }

    public function addCarsWithCurrent(Car $carsWithCurrent): self
    {
        if (!$this->carsWithCurrent->contains($carsWithCurrent)) {
            $this->carsWithCurrent[] = $carsWithCurrent;
            $carsWithCurrent->addRelatedCar($this);
        }

        return $this;
    }

    public function removeCarsWithCurrent(Car $carsWithCurrent): self
    {
        if ($this->carsWithCurrent->removeElement($carsWithCurrent)) {
            $carsWithCurrent->removeRelatedCar($this);
        }

        return $this;
    }

    public function toArray(): array
    {
        $result = [];
        foreach ($this as $key=>$value) {
            if ($key === 'images') {
                /**@var Image[] $value*/

                foreach ($value as $image) {
                    $result[$key][]=$image->toArray();
                }

            } else {
                $result[$key]=$value;
            }

        }
        return $result;
    }

    public function getPowerReserve(): ?string
    {
        return $this->powerReserve;
    }

    public function setPowerReserve(string $powerReserve): self
    {
        $this->powerReserve = $powerReserve;

        return $this;
    }

    public function getBatteryCapacity(): ?string
    {
        return $this->batteryCapacity;
    }

    public function setBatteryCapacity(string $batteryCapacity): self
    {
        $this->batteryCapacity = $batteryCapacity;

        return $this;
    }
}
