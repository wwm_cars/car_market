<?php

namespace App\Controller;

use App\Entity\Banner;
use App\Entity\Car;
use App\Entity\Theme;
use App\Repository\CarRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class CarController
 * @package App\Controller
 */
class CarController extends AbstractController
{

    /**
     * @Route("/car/listing/{offset}", name="carListing")
     *
     * @param int $offset
     * @param SerializerInterface $serializer
     *
     * @return Response
     */
    public function loadCarsListingPage(int $offset, SerializerInterface $serializer): Response
    {
        $carRepository = $this->getDoctrine()->getRepository(Car::class);
        $cars = $carRepository->findAll();

        $offsetCars = array_slice($cars, $offset*10, 10);
        $recentCars = array_slice($cars, -3);
        $paginationText = sprintf(
            "Показано %d-%d из %d",
            $offset>0 ? $offset*10+1: 1,
            $offset>0 ? $offset*10+10: 10,
            count($cars)
        );
        $pagination = range(0, count($cars)/10);

        return $this->render(
            'car/cars_listing.html.twig',
            [
                'Cars' => $offsetCars,
                'RecentСars'=>$recentCars,
                'PaginationText'=>$paginationText,
                'Pagination'=>$pagination,
                'Offset'=>$offset,
                'Theme' => $this->loadTheme(),
                'CarsItem'=>$this->carsItems($cars),
            ]
        );
    }

    private function loadFooterBanner(): ?Banner
    {
        return $this->getDoctrine()
            ->getRepository(Banner::class)
            ->findOneBy(['place' => Banner::FOOTER_BANNER], ['show_order' => 'ASC']);
    }

    /**
     * @return Banner|null
     */
    private function loadSubBanner(): ?Banner
    {
        return $this->getDoctrine()
            ->getRepository(Banner::class)
            ->findOneBy(['place' => Banner::HEADER_BANNER], ['show_order' => 'ASC']);
    }

    /**
     * @Route("/car/details/{id}", name="carDetails")
     * @param int $id
     * @return Response
     */
    public function loadCarDetailsPage(int $id): Response
    {
        $carRepository = $this->getDoctrine()->getRepository(Car::class);
        $car = $carRepository->findOneBy(['id'=>$id]);
        return $this->render('car/car_details.html.twig', [
            'Car' => $car,
            'Theme' => $this->loadTheme()
        ]);
    }

    /**
     * @Route("/car/listLeftSidebar/{offset}", name="carListLeftSidebar")
     *
     * @param int $offset
     */
    public function loadCarListLeftSidebarPage(int $offset): Response
    {
        $carRepository = $this->getDoctrine()->getRepository(Car::class);
        $cars = $carRepository->findAll();

        $offsetCars = array_slice($cars, $offset*10, 10);
        $recentCars = array_slice($cars, -3);
        $paginationText = sprintf(
            "Показано %d-%d из %d",
            $offset>0 ? $offset*10+1: 1,
            $offset>0 ? $offset*10+10: 10,
            count($cars)
        );



        /*$jsonCars = [];
        foreach ($cars as $car) {
            $jsonCars[] =  $serializer->serialize($car, 'json');
        }*/
        $pagination = range(0, count($cars)/10);

        return $this->render(
            'car/car-list-leftsidebar.html.twig',
            [
                'Cars' => $offsetCars,
                'RecentСars'=>$recentCars,
                'PaginationText'=>$paginationText,
                'Pagination'=>$pagination,
                'Offset'=>$offset,
                'Theme' => $this->loadTheme(),
                'CarsItem'=>$this->carsItems($cars),
            ]);
    }

    private function carsItems(array $cars)
    {
        $carsItem = [];
        /**@var $car Car*/
        foreach ($cars as $car) {
            $model = trim($car->getModel());
            $brand = trim($car->getBrand());
            $body_type = trim($car->getBodyType());
            $transmission = trim($car->getTransmission());
            $type = trim($car->getType());
            $carsItem['cars_names'][$brand][$model]=0;
            $carsItem['body_type'][$body_type]=0;
            $carsItem['transmission'][$transmission]=0;
            $carsItem['type'][$type]=0;
        }

        ksort($carsItem);
        $carsItem = array_map(function(array $carItem) {
            ksort($carItem);
            return $carItem;
        },
            $carsItem
        );

        return$carsItem;
    }

    private function loadTheme(): string
    {
        /** @var Theme $theme */
        $theme = $this->getDoctrine()
            ->getRepository(Theme::class)
            ->findOneBy([], ['id' => 'ASC']);
        if ($theme) {
            return $theme->getTheme();
        }
        return 'yellow';
    }
}
