function inflateHomepageCarBox(car) {
    var id = car.id;
    if (car.status !== 0) {
        var label= '<div class="tag-2 bg-active">Топ</div>';
    } else {
        var label = '<div class="for">'+car.type+'</div>';
    }

    if (car.price > 0) {
        var price = '$ '+car.price;
    } else {
        var price = 'Цену уточняйте';
    }
    var brand = car.brand;
    var model = car.model;
    var image;

    var images = car.images
    if (images !== undefined && images.length !== 0) {
        for (var key in car.images) {
            if (car.images[key].isAvatar) {
                image = '/uploads/images/'+car.images[key].image;
            }
        }
        if (image === undefined) {
            image = '/uploads/images/'+car.images[0].image;
        }
    } else {
        image = '/img/car/car-1.png';
    }

//
    var template =
        '<div class="col-lg-4 col-md-6">'+
        '<div class="car-box-3">'+
        '<a href="/car/details/'+id+'" class="car-img">'+
        '<div class="car-thumbnail">'+
        label+
        '<div class="price-box">'+
        '<br>'+
        '<span>'+price+'</span>'+
        '</div>'+
        ' <img class="d-block w-100" src="'+image+'" alt="car">'+
        '</div>'+
        '</a>'+
        '<div class="detail">'+
        '<h1 class="title">'+
        ' <a href="/car/details/'+id+'">'+brand +' '+model+'</a>'+
        '</h1>'+
        '<ul class="custom-list">'+
        '<li>'+
        '<a href="#">'+brand+'</a> /'+
        '</li>'+
        '<li>'+
        '<a href="#">'+car.type +'</a>'+
        '</li>'+
        '</ul>'+
        '<ul class="facilities-list clearfix">'+
        ' <li>'+
        '<i class="flaticon-fuel"></i>'+ car.fuel+
        '</li>'+
        ' <li>'+
        '<i class="flaticon-way"></i>'+ car.way +'km'+
        ' </li>'+
        '<li>'+
        '<i class="flaticon-manual-transmission"></i>'+ car.transmission+
        '</li>'+
        '<li>'+
        '<i class="flaticon-car"></i>'+car.equipment+
        '</li>'+
        '<li>'+
        '<i class="flaticon-gear"></i>'+car.color+
        '</li>'+
        '<li>'+
        '<i class="flaticon-calendar-1"></i>'+car.year+
        '</li>'+
        '</ul>'+
        '</div>'+
        '</div>'+
        '</div>';

    return template;
}

function inflateListingCarBox(car) {
    var brand = car.brand;
    var model = car.model;
    var image;
    var id = car.id;

    var images = car.images
    if (images !== undefined && images.length !== 0) {
        for (var key in car.images) {
            if (car.images[key].isAvatar) {
                image = '/uploads/images/'+car.images[key].image;
            }
        }
        if (image === undefined) {
            image = '/uploads/images/'+car.images[0].image;
        }
    } else {
        image = '/img/car/car-1.png';
    }
    if (car.price > 0) {
        var price = '$ '+car.price;
    } else {
        var price = 'Цену уточняйте';
    }

    var template ='<div class="col-lg-6 col-md-6">'+
        '<div class="car-box-3">'+
    ' <a class="car-img" href="/car/details/'+id+'">'+
                '<div class="car-thumbnail">'+
                   ' <div class="for">'+car.type+'</div>'+
                    '<div class="price-box">'+
                        '<br>'+
                            '<span> '+price+'</span>'+
                    '</div>'+
                    ' <img class="d-block w-100" src="'+image+'" alt="car">'+
                '</div>'+
            '</a>'+
            '<div class="detail">'+
                '<h1 class="title">'+
        ' <a href="/car/details/'+id+'">'+brand +' '+model+'</a>'+
                '</h1>'+
                '<ul class="custom-list">'+
                    '<li>'+
                       ' <a href="#">'+car.brand+'</a> /'+
                    '</li>'+
                    '<li>'+
                       ' <a href="#">'+car.type +'</a>'+
                    '</li>'+
                '</ul>'+
                '<ul class="facilities-list clearfix">'+
                    '<li>'+
                      '<i class="flaticon-fuel"></i>'+ car.fuel+
                    '</li>'+
                    '<li>'+
                        '<i class="flaticon-way"></i> '+ car.way+' <a>km</a>'+
                    '</li>'+
                    '<li>'+
                        '<i class="flaticon-manual-transmission"></i> '+car.transmission+
                    '</li>'+
                    '<li>'+
                        '<i class="flaticon-car"></i> '+car.equipment+
                    '</li>'+
                    '<li>'+
                        '<i class="flaticon-gear"></i> '+car.color+
                    '</li>'+
                    '<li>'+
                        '<i class="flaticon-calendar-1"></i> '+car.year+
                    '</li>'+
                '</ul>'+
            '</div>'+
        '</div>'+
    '</div>';

    return template;
}

function inflateLeftsidebarCarBox(car) {
    var brand = car.brand;
    var model = car.model;
    var image;
    var id = car.id;

    var images = car.images
    if (images !== undefined && images.length !== 0) {
        for (var key in car.images) {
            if (car.images[key].isAvatar) {
                image = '/uploads/images/'+car.images[key].image;
            }
        }
        if (image === undefined) {
            image = '/uploads/images/'+car.images[0].image;
        }
    } else {
        image = '/img/car/car-1.png';
    }
    if (car.price > 0) {
        var price = '$ '+car.price;
    } else {
        var price = 'Цену уточняйте';
    }

    var template = '<div class="car-box-2">'+
        '<div class="row">'+
            '<div class="col-lg-5 col-md-5 col-pad">'+
                ' <a class="car-img" href="/car/details/'+id+'">'+
                    '<div class="car-thumbnail">'+
                        '<div class="for">'+car.type+'</div>'+
                        '<div class="price-box">'+
                            '<br>'+
                               ' <span> '+price+'</span>'+
                        '</div>'+
                        ' <img class="d-block w-100" src="'+image+'" alt="car">'+
                    '</div>'+
                '</a>'+
            '</div>'+
            '<div class="col-lg-7 col-md-7 col-pad align-self-center">'+
                '<div class="detail">'+
                    '<h3 class="title">'+
                        ' <a href="/car/details/'+id+'">'+brand +' '+model+'</a>'+
                    '</h3>'+
                    '<ul class="custom-list">'+
                        '<li>'+
                            '<a href="#">'+brand+'</a> /'+
                        '</li>'+
                        '<li>'+
                            '<a href="#">'+car.type+'</a>'+
                        '</li>'+
                    '</ul>'+
                    '<ul class="facilities-list clearfix">'+
                        '<li>'+
                           ' <i class="flaticon-way"></i> '+car.way+' km'+
                        '</li>'+
                        '<li>'+
                            '<i class="flaticon-manual-transmission"></i> '+car.transmission+
                        '</li>'+
                        '<li>'+
                            '<i class="flaticon-calendar-1"></i>'+ car.year+
                        '</li>'+
                        '<li>'+
                            '<i class="flaticon-fuel"></i>'+car.fuel+
                        '</li>'+
                        '<li>'+
                           ' <i class="flaticon-car"></i> '+car.equipment+
                        '</li>'+
                        '<li>'+
                           ' <i class="flaticon-gear"></i> '+car.getColor+
                        '</li>'+
                    '</ul>'+
                '</div>'+
            '</div>'+
        '</div>'+
    '</div>';

    return template;
}