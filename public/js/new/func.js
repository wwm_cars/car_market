function setDefaultOptions() {

    $("#brand").select2();

    $(".select2").addClass("form-control form-control-lg col");
    $(".select2-selection--single").css("border-style", "none");
    $(".select2").css("width", "100%");
    $("#brand2").select2();
}


function changeModel(itemCars) {
    var brand =  $('#brand').val();
    var modelSelect = $("#model");

    modelSelect.empty();
    modelSelect.append(new Option("Модель", ""));
    var models = itemCars[brand];

    if (models === undefined) {
        modelSelect.prop( "disabled", true);
        return;
    }
    modelSelect.prop( "disabled", false);

    for (var key in models) {
        modelSelect.append(new Option(key, key));
    }

    modelSelect.select2();
    $(".select2").addClass("form-control form-control-lg");
    $(".select2-selection--single").css("border-style", "none")
}

function searchBox(itemCars) {
    var brand =  $('#brand').val();
    var barndObj = itemCars[brand];

    var modelObj;
    var findCarsRequest= [];
    if (barndObj===undefined) {
        modelObj = undefined;
    } else {
        findCarsRequest.push({brand: brand});
        var model = $("#model").val();
        modelObj = barndObj[model];
        if (modelObj !==undefined) {
            findCarsRequest.push({model: model});
        }

    }

    var minPriceVal = $("#min-price").val();
    var maxPriceVal = $("#max-price").val();
    var minPrice = 0;
    var maxPrice = 0;

    if (minPriceVal !== "") {
        minPrice = parseInt(minPriceVal);
        findCarsRequest.push({MinPrice: minPrice});
    }
    if (maxPriceVal !== "") {
        maxPrice = parseInt(maxPriceVal);
        findCarsRequest.push({MaxPrice: maxPrice});
    }
    if (barndObj===undefined && modelObj===undefined && minPrice===0 && maxPrice===0) {
        return;
    }

    findCarsRequest = JSON.stringify(findCarsRequest);
    $.ajax({
        type: "POST",
        url: '/search/box',
        data: findCarsRequest,
        success: function (response) {
            response = jQuery.parseJSON(response);

            var htmlstr = '';
            if (response.length === 0) {
                htmlstr= '<div class="main-title"><h1>По вашему запросу ничего не найдено</h1></div>';
            } else {
                var htmlstrres = ''
                htmlstr+= '<div class="main-title"><h1>Найдено '+response.length+' результатов</h1></div>';
                for (var key in response) {
                    htmlstrres+= inflateHomepageCarBox(response[key]);
                }
                htmlstr += '<div class="container"><div class="row">'+htmlstrres+'</div></div>';
                console.log(htmlstr)
            }
            $('#content-area').empty();
            $(htmlstr).appendTo('#content-area');

        }
    });
}



function advancedSearchBox(itemCars) {
    var page = $('#button-search').val();
    console.log(page)
    var findCarsRequest= [];
    var body_type =  $('#body_type').val();
    var body_typeObj = itemCars['body_type'][body_type];
    if (body_typeObj !==undefined) {
        findCarsRequest.push({body_type: body_type});
    }

    var transmission =  $('#transmission').val();
    var transmissionObj = itemCars['transmission'][transmission];
    if (transmissionObj !==undefined) {
        findCarsRequest.push({transmission: transmission});
    }

    var type =  $('#type').val();
    var typeObj = itemCars['type'][type];
    if (typeObj !==undefined) {
        findCarsRequest.push({type: type});
    }

    var advanceItemCars = itemCars['cars_names'];

    var brand =  $('#brand').val();
    var barndObj = advanceItemCars[brand];
    var modelObj;

    if (barndObj===undefined) {
        modelObj = undefined;
    } else {
        findCarsRequest.push({brand: brand});
        var model = $("#model").val();
        modelObj = barndObj[model];
        if (modelObj !==undefined) {
            findCarsRequest.push({model: model});
        }

    }

    var minPriceVal = $(".current-min").val();
    var maxPriceVal = $(".current-max").val();
    var minPrice = 0;
    var maxPrice = 0;

    if (minPriceVal !== undefined) {
        minPrice = parseInt(minPriceVal);
        findCarsRequest.push({MinPrice: minPrice});
    }
    if (maxPriceVal !== undefined) {
        maxPrice = parseInt(maxPriceVal);
        findCarsRequest.push({MaxPrice: maxPrice});
    }
    if (barndObj===undefined && modelObj===undefined && minPrice===0 && transmissionObj===undefined && body_typeObj===undefined && typeObj===undefined) {
        return;
    }

    findCarsRequest = JSON.stringify(findCarsRequest);
    $.ajax({
        type: "POST",
        url: '/search/box',
        data: findCarsRequest,
        success: function (response) {
            response = jQuery.parseJSON(response);

            var htmlstr = '';
            if (response.length === 0) {
                htmlstr= '<h1>По вашему запросу ничего не найдено</h1>';
            } else {
                var htmlstrres = ''
                htmlstr+= '<h1>Найдено '+response.length+' результатов</h1>';
                for (var key in response) {
                    if (page === 'car-listing') {
                        htmlstrres+= inflateListingCarBox(response[key]);
                    } else {
                        htmlstrres+= inflateLeftsidebarCarBox(response[key]);
                    }
                }
                htmlstr +=htmlstrres;
                console.log(htmlstr)
            }
            $('#content-area').empty();
            $('.pagination').empty();
            $(htmlstr).appendTo('#content-area');

        }
    });
}