<?php

namespace App\Entity;

use App\Repository\ImageRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=ImageRepository::class)
 * @Vich\Uploadable
 */
class Image
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="image", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $isAvatar;

    /**
     * @ORM\ManyToOne(targetEntity=Car::class, inversedBy="images")
     * @ORM\JoinColumn(nullable=false)
     */
    private $car;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->isAvatar = false;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeImmutable|null $createdAt
     * @return $this
     */
    public function setCreatedAt(?\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getCar()
    {
        return $this->car;
    }

    public function setCar($car): self
    {
        $this->car = $car;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @param File|null $imageFile
     */
    public function setImageFile(?File $imageFile): void
    {
        $this->imageFile = $imageFile;
    }

    /**
     * @return bool
     */
    public function isAvatar(): bool
    {
        return $this->isAvatar;
    }

    /**
     * @param bool $isAvatar
     * @return $this
     */
    public function setIsAvatar(bool $isAvatar): self
    {
        $this->isAvatar = $isAvatar;
        return $this;
    }

    public function __toString()
    {
        $message = $this->id.'# '.$this->image;
        if ($this->isAvatar) {
            $message .= ', AVATAR';
        }
        return $message;
    }

    public function getIsAvatar(): ?bool
    {
        return $this->isAvatar;
    }

    public function __string(): array
    {
        $result = [];
        foreach ($this as $key=>$value) {
            $result[$key]=$value;
        }
        return $result;
    }

    public function toArray(): array
    {
        $result = [];
        foreach ($this as $key=>$value) {
            $result[$key]=$value;
        }
        return $result;
    }
}
