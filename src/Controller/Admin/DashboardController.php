<?php

namespace App\Controller\Admin;

use App\Entity\Banner;
use App\Entity\Car;
use App\Entity\Image;
use App\Entity\Theme;
use App\Entity\Video;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_ADMIN")
 */
class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('bundles/EasyAdminBundle/welcome.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Car Market');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToCrud('Машины', 'fas fa-car', Car::class);
        yield MenuItem::linkToCrud('Баннеры', 'fas fa-image', Banner::class);
        yield MenuItem::linkToCrud('Тема', 'fas fa-eye', Theme::class);
        yield MenuItem::linkToCrud('Видео', 'fas fa-video', Video::class);
        yield MenuItem::section();
        yield MenuItem::linktoRoute('Назад к сайту', 'fas fa-home', 'homepage');
    }
}
