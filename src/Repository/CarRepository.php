<?php

namespace App\Repository;

use App\Entity\Car;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Car|null find($id, $lockMode = null, $lockVersion = null)
 * @method Car|null findOneBy(array $criteria, array $orderBy = null)
 * @method Car[]    findAll()
 * @method Car[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CarRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Car::class);
    }

    // /**
    //  * @return Car[] Returns an array of Car objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Car
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function save(Car $car): int
    {
        $sql = 'insert into car (diller_id, brand, model, equipment, price, year, full_data, status, car_image_id, transmission, color, fuel, way, body_type, type, created_at, updated_at) 
                    value (:diller_id, :brand, :model, :equipment, :price, :year, :full_data, :status, :car_image_id, :transmission, :color, :fuel, :way, :body_type, :type, :created_at, :updated_at)';

        $conn = $this->getEntityManager()->getConnection();
        $placeholder = [
            'diller_id'=>$car->getDillerId(),
            'brand'=>$car->getBrand(),
            'model'=>$car->getModel(),
            'equipment'=>$car->getEquipment(),
            'price'=>$car->getPrice(),
            'year'=>$car->getYear(),
            'full_data'=>json_encode($car->getFullData()),
            'status'=>$car->getStatus(),
            'car_image_id'=>$car->getCarImageId(),
            'transmission'=>$car->getTransmission(),
            'color'=>$car->getColor(),
            'fuel'=>$car->getFuel(),
            'way'=>$car->getWay(),
            'body_type'=>$car->getBodyType(),
            'type'=>$car->getType(),
            'created_at'=>$car->getCreatedAt()->format('Y-m-d H:i:s'),
            'updated_at' => $car->getUpdatedAt()->format('Y-m-d H:i:s')
        ];

        $stmt = $conn->prepare($sql);
        $stmt->executeQuery($placeholder);

        return (int)$conn->lastInsertId();
    }

    public function findUniqueFieldsData(string $columnName): array
    {
        $sql = 'select distinct '.$columnName.' from car order by '.$columnName.' desc';

        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $result = $stmt->executeQuery();
        $rows = $result->fetchAllAssociative();
        $result = [];
        foreach ($rows as $row) {
            $result[] = $row[$columnName];
        }
        return $result;
    }

    public function findUniqueKeyValues(string $key): array
    {
        $sql = 'select distinct trim(full_data->>\'$."'.$key.'"\') AS data from car where full_data like :likeKey';

        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $result = $stmt->executeQuery([
            'likeKey' => '%' . $key . '%'
        ]);
        $rows = $result->fetchAllAssociative();
        $result = [];
        foreach ($rows as $row) {
            $result[] = $row['data'];
        }
        return $result;
    }

    public function findDataForFillCharacteristics(): array
    {
        $sql = 'select id, full_data
        from car
        where full_data <> "" and id not in (select car_id from characteristic)';

        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $result = $stmt->executeQuery();
        $rows = $result->fetchAllAssociative();
        $result = [];
        foreach ($rows as $row) {
            $res = [];
            foreach (json_decode($row['full_data'], true) as $item) {
                $res[key($item)] = current($item);
            }

            $result[(int) $row['id']] = $res;
        }
        return $result;
    }

    public function findByCriteria(array $criterias)
    {
        $sql = 'select id
            from car ';

        $params = [];
        foreach ($criterias as $key=>$criteria) {
            if ($key === 'MinPrice') {
                $params[] = "price >= :{$key}";
                continue;
            }
            if ($key === 'MaxPrice') {
                $params[] = "price <= :{$key}";
                continue;
            }

            $params[] = "{$key} like :{$key}";
            $criterias[$key] = "%$criteria%";
        }

        $sql = sprintf('%s where %s',$sql, implode(' and ', $params));

        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $result = $stmt->executeQuery($criterias);

        return $result->fetchAll();
    }
}
