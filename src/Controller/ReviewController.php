<?php

namespace App\Controller;

use App\Entity\Car;
use App\Entity\Theme;
use App\Entity\Video;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReviewController extends AbstractController
{
    /**
     * @Route("/review", name="review")
     */
    public function loadReview(): Response
    {
        $videoRepository = $this->getDoctrine()->getRepository(Video::class);
        $videos = $videoRepository->findAll();

        return $this->render('video/review.html.twig', [
            'Theme' => $this->loadTheme(),
            'Videos' =>$videos
        ]);
    }

    private function loadTheme(): string
    {
        /** @var Theme $theme */
        $theme = $this->getDoctrine()
            ->getRepository(Theme::class)
            ->findOneBy([], ['id' => 'ASC']);
        if ($theme) {
            return $theme->getTheme();
        }
        return 'yellow';
    }
}
