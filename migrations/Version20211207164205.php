<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211207164205 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE related_car (car_id INT NOT NULL, related_car_id INT NOT NULL, INDEX IDX_6BAFDC58C3C6F69F (car_id), INDEX IDX_6BAFDC586F0A8E39 (related_car_id), PRIMARY KEY(car_id, related_car_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE related_car ADD CONSTRAINT FK_6BAFDC58C3C6F69F FOREIGN KEY (car_id) REFERENCES car (id)');
        $this->addSql('ALTER TABLE related_car ADD CONSTRAINT FK_6BAFDC586F0A8E39 FOREIGN KEY (related_car_id) REFERENCES car (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE related_car');
    }
}
