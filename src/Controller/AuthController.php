<?php

namespace App\Controller;

use App\Entity\Theme;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Repository\UserRepository;
use App\Entity\User;

class AuthController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $request->query->get('username');
        if ($lastUsername === '') {
            // last username entered by the user
            $lastUsername = $authenticationUtils->getLastUsername();
        }

        return $this->render('auth/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
            'Theme' => $this->loadTheme()
        ]);
    }

    /**
     * @Route("/register", name="register", methods={"GET","HEAD"})
     */
    public function registerForm(): Response
    {
        return $this->render('auth/register.html.twig', [
            'controller_name' => 'AuthController',
            'Theme' => $this->loadTheme()
        ]);
    }

    /**
     * @Route("/register", name="register_save", methods={"POST"})
     */
    public function register(Request $request, UserPasswordHasherInterface $passwordHasher, UserRepository $repository): Response
    {
        $data = json_decode($request->getContent(), true);
        $user = $repository->findOneBy(['email' => $data["username"]]);
        if ($user) {
            return new JsonResponse([
                'Result' => -1,
                'Message' => 'Такой пользователь уже существует'
            ]);
        }

        $user = new User();
        $user->setEmail($data["username"]);
        $user->setFullName($data["fullName"]);
        $user->setRoles(['ROLE_USER']);
        $hashedPassword = $passwordHasher->hashPassword(
            $user,
            $data["password"]
        );
        $user->setPassword($hashedPassword);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return new JsonResponse([
            'Result' => 10,
            'Message' => 'Пользователь успешно создан'
        ]);
    }

    /**
     * @Route("/forgotPassword", name="forgotPassword")
     */
    public function forgot(): Response
    {
        return $this->render('auth/forgotPassword.html.twig', [
            'controller_name' => 'AuthController',
            'Theme' => $this->loadTheme()
        ]);
    }

    /**
     * @Route("/logout", name="logout", methods={"GET"})
     */
    public function logout(): Response
    {
        throw new \Exception('Don\'t forget to activate logout in security.yaml');
    }

    private function loadTheme(): string
    {
        /** @var Theme $theme */
        $theme = $this->getDoctrine()
            ->getRepository(Theme::class)
            ->findOneBy([], ['id' => 'ASC']);
        if ($theme) {
            return $theme->getTheme();
        }
        return 'yellow';
    }
}
