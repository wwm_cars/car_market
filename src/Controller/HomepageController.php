<?php

namespace App\Controller;

use App\Entity\Banner;
use App\Entity\Car;
use App\Entity\Theme;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    /**
     * @Route("/", name="homepage", schemes={"http"})
     */
    public function index(): Response
    {
        $carRepository = $this->getDoctrine()->getRepository(Car::class);
        $cars = $carRepository->findAll();
        /**@var $car Car*/
        $carsItem = [];
        foreach ($cars as $car) {
            $model = trim($car->getModel());
            $brand = trim($car->getBrand());
            $carsItem['cars_names'][$brand][$model]=0;
        }

        ksort($carsItem);
        $carsItem = array_map(function(array $carItem) {
             ksort($carItem);
             return $carItem;
        },
            $carsItem
        );

        return $this->render('homepage/index.html.twig', [
            'Cars'=>$cars,
            'CarsItem'=>$carsItem,
            'Theme' => $this->loadTheme()
        ]);
    }

    private function loadTheme(): string
    {
        /** @var Theme $theme */
        $theme = $this->getDoctrine()
            ->getRepository(Theme::class)
            ->findOneBy([], ['id' => 'ASC']);
        if ($theme) {
            return $theme->getTheme();
        }
        return 'yellow';
    }

    /**
     * @Route("/search", name="search")
     */
    public function search(Request $request): Response
    {
        $searchData = strtolower($request->request->get('search_data'));
        $carRepository = $this->getDoctrine()->getRepository(Car::class);
        $cars = $carRepository->findAll();

        /**@var $car Car*/
        $result = [];
        foreach ($cars as $car) {
            $similarBrand = 0;
            $similarModel = 0;
            $similarBrandModel = 0;
            similar_text($searchData, strtolower($car->getBrand()), $similarBrand);
            similar_text($searchData, strtolower($car->getModel()), $similarModel);
            similar_text($searchData, strtolower($car->getBrand().' '.$car->getModel()), $similarBrandModel);

            if ($similarBrand>=70 or $similarModel>=70 or $similarBrandModel>=40) {
                $result[] = $car;
            }
        }

        return $this->render('homepage/search.html.twig', [
            'Cars' => $result,
            'Theme' => $this->loadTheme()
        ]);
    }

    /**
     * @Route("/search/box", name="search")
     */
    public function searchBox(Request $request): Response
    {
        $result = [];
        $criteria = [];
        $crit = json_decode($request->getContent(false), true);
        foreach ($crit as $item) {
            $criteria[key($item)]=current($item);
        }


        if ($criteria !== false or $criteria !== null) {
            $maxPrice = $criteria['MaxPrice'] ?? 0;
            $minPrice = $criteria['MinPrice'] ?? 0;
            if ($maxPrice < $minPrice) {
                $maxPrice = $criteria['MinPrice'] ?? 0;
                $minPrice = $criteria['MaxPrice'] ?? 0;
                $criteria['MinPrice'] = $minPrice;
                $criteria['MaxPrice'] = $maxPrice;
            }
            
            $ids = $this->getDoctrine()
                ->getRepository(Car::class)
                ->findByCriteria($criteria);
            $cars= [];
            foreach ($ids as $id) {
                $idPrepare = (int)(current($id));
                $cars[]=  $this->getDoctrine()
                    ->getRepository(Car::class)
                    ->findOneBy(['id'=>$idPrepare]);
            }


            /**@var $car Car*/
            foreach ($cars as $car) {
                $result[]= $car->toArray();
            }
        }

        return $this->json(json_encode($result));
    }
}
